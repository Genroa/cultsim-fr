# Cultsim-FR

Repository du mod "Traduction Française" pour Cultist Simulator, disponible sur le Steam Workshop.

## Comment contribuer
- Installez le mod en local (voir section suivante, version "via Git") 
- créez une nouvelle branche à partir de la branche principale dans votre client git
- ouvrez les fichiers JSON avec votre éditeur de texte favori (pas Word). VSCode est recommandé, Notepad++ fait aussi l'affaire.
 > Alternative: utilisez l'outil https://genroa.github.io/Cultist-Simulator-Translation-Tool/ pour traduire plus facilement les choses. Les fichiers que l'outil produit ne sont pas parfaits (c'est une copie parfaite des fichiers d'origine alors que seuls certains champs sont nécessaires), mais son interface rassurera peut-être ceux qui sont effrayés par l'édition en brut de fichiers textes.
- utilisez votre client git pour créer une pull request de votre branche > vers la branche principale
- j'évalue et merge chaque branche manuellement pour éviter tout acte malveillant. N'y voyez pas du gatekeeping de ma part, si votre travail est bon (points bonus s'il couvre des bouts pas couverts à l'heure actuelle) il sera mergé. :)
***
## Utiliser le mod pour la version GOG / Installer le mod en local / Tester vos changements en local

Deux méthodes possibles, selon votre usage.

### Comment trouver le dossier des mods sur sa machine?
 > Pour trouver le dossier des mods, ouvrez votre jeu, ouvrez le menu des paramètres dans le menu principal, et cliquez sur le gros bouton violet "Browse Files" en bas. Il vous ouvrira le dossier des données locales. Si un dossier "mods" n'existe pas à cet endroit, créez le et mettez le dossier du repo dedans.

### Pour non contributeurs: Installation locale rapide
> **Avantage:** moins technique, vous avez juste à dézipper un dossier au bon endroit.

> **Inconvénient:** ne convient pas pour les utilisateurs voulant proposer des améliorations.
- Cliquez sur le gros bouton de téléchargement ("flèche vers le bas") sur la page principale de ce repo, en haut. Choisissez "zip".
- Dézippez l'archive dans le dossier mods du jeu.
- Assurez vous que vous n'avez pas la version du workshop du mod d'activée
- Activez le mod dans le menu principal du jeu ("Sixth History")

### Pour contributeurs: Installation en local (via Git)
> **Avantage:** vous pouvez mettre votre version locale à jour en un clic via votre client Git.

> **Inconvénient:** plus technique à utiliser. **Unique méthode viable pour les utilisateurs voulant proposer des améliorations.**
- Utilisez un client git (recommandé: Gitkraken, ou le client intégré à VSCode) pour télécharger ce repository
- Placez-le dans le dossier des mods locaux
- Assurez vous que vous n'avez pas la version du workshop du mod d'activée
- Activez le mod local dans le menu principal du jeu ("Sixth History")

Vous utilisez maintenant la version locale du mod. Tout changement que vous ferez à ses fichiers seront visibles en jeu après un redémarrage.

### Mettre à jour sa version locale
- Si vous avez utilisé la méthode de téléchargement du zip, téléchargez le à nouveau et remplacez le dossier.
- Si vous avez utilisé la méthode via git, utilisez votre client git pour pull la version la plus à jour de ce repo (branche main).
- C'est tout. :)

***

## Progression
- Le jeu de base est couvert en majorité, avec des trous notables dans les romances et les actions de conversation
- Les DLCs ne sont pas encore couverts à l'exception du Prêtre
- Le new game+ (apôtres) n'est que peu couvert

Mods couverts:
- [High Society](https://steamcommunity.com/sharedfiles/filedetails/?id=2473246640)
- [Les options de The Roost Machine](https://steamcommunity.com/sharedfiles/filedetails/?id=2625527332)
- [Les options de Improved Camera](https://steamcommunity.com/sharedfiles/filedetails/?id=2778988594)